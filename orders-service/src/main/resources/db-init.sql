DROP TABLE IF EXISTS Orders;
DROP TABLE IF EXISTS Order_Status;

CREATE TABLE Order_Status
(
    id     INT         NOT NULL,
    status VARCHAR(10) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE Orders
(
    id          BIGINT         NOT NULL,
    customer_id BIGINT         NOT NULL,
    status_id   INT            NOT NULL,
    amount      NUMERIC(19, 2) NOT NULL,
    placed_date TIMESTAMP      NOT NULL,
    change_date TIMESTAMP      NOT NULL,
    version     INT            NOT NULL,
    PRIMARY KEY (id, version),
    FOREIGN KEY (status_id) REFERENCES Order_Status (id)
);

INSERT
INTO Order_Status
VALUES (1, 'Placed');

INSERT
INTO Order_Status
VALUES (2, 'Accepted');

INSERT
INTO Order_Status
VALUES (3, 'Rejected');

INSERT
INTO Orders
VALUES (1, 1, 1, 500, '2019-04-29 10:23:54', '2019-04-29 10:23:54', 1);

INSERT
INTO Orders
VALUES (1, 1, 2, 500, '2019-04-29 10:23:54', '2019-04-29 10:25:00', 2);
package com.lideo.interview.orderservice.entities;

import com.lideo.interview.orderservice.model.OrderStatus;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "Orders")
@Data
@NoArgsConstructor
public class Order {

    @EmbeddedId
    private OrderId orderId;

    @Column(name = "customer_id")
    private long customerId;

    @NonNull
    @Column(name = "status_id")
    @Enumerated(EnumType.ORDINAL)
    private OrderStatus status;

    @NonNull
    private BigDecimal amount;

    @NonNull
    private LocalDateTime placedDate;

    @NonNull
    private LocalDateTime changeDate;
}

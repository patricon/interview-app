package com.lideo.interview.orderservice.repositories;

import com.lideo.interview.orderservice.entities.Order;
import com.lideo.interview.orderservice.entities.OrderId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface OrderRepository extends CrudRepository<Order, OrderId> {

    @NonNull
    List<Order> findAll();

    Optional<Order> findFirstByOrderIdIdOrderByOrderIdVersionDesc(long id);

    Optional<Order> findFirstByOrderByOrderIdIdDesc();
}

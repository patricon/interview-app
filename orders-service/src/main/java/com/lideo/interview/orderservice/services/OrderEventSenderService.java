package com.lideo.interview.orderservice.services;

import com.lideo.interview.orderservice.entities.Order;
import com.lideo.interview.orderservice.model.OrderEvent;
import com.lideo.interview.orderservice.model.OrderStatus;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderEventSenderService {
    private RabbitTemplate template;

    private DirectExchange ordersExchange;

    public OrderEventSenderService(@Autowired RabbitTemplate template, @Autowired DirectExchange ordersExchange) {
        this.template = template;
        this.ordersExchange = ordersExchange;
    }

    public void send(Order order, OrderStatus oldStatus) {
        OrderEvent event = new OrderEvent();
        event.setNewState(order.getStatus());
        event.setPreviousState(oldStatus);
        event.setOrderAmount(order.getAmount());
        event.setOrderId(order.getOrderId().getId());
        event.setCustomerId(order.getCustomerId());

        template.convertAndSend(ordersExchange.getName(), order.getStatus().toString(), event);
    }
}

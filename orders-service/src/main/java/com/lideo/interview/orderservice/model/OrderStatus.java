package com.lideo.interview.orderservice.model;

public enum OrderStatus {
    NONE, PLACED, ACCEPTED, REJECTED
}

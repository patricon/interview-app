package com.lideo.interview.orderservice.services;

import com.lideo.interview.orderservice.entities.Order;
import com.lideo.interview.orderservice.entities.OrderId;
import com.lideo.interview.orderservice.exceptions.NotExistingOrderException;
import com.lideo.interview.orderservice.model.OrderStatus;
import com.lideo.interview.orderservice.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {


    private OrderRepository orderRepository;

    private OrderEventSenderService orderEventSenderService;

    public OrderService(@Autowired OrderRepository orderRepository,
                        @Autowired OrderEventSenderService orderEventSenderService) {
        this.orderRepository = orderRepository;
        this.orderEventSenderService = orderEventSenderService;
    }

    public Order placeOrder(Order order) {
        final int INITIAL_VERSION = 1;

        OrderId orderId = new OrderId(getCurrentId(), INITIAL_VERSION);
        order.setOrderId(orderId);

        LocalDateTime currentTime = LocalDateTime.now();
        order.setPlacedDate(currentTime);
        order.setChangeDate(currentTime);

        order.setStatus(OrderStatus.PLACED);

        Order saved = orderRepository.save(order);
        orderEventSenderService.send(saved, OrderStatus.NONE);

        return saved;
    }

    public Order changeStatus(long orderId, OrderStatus status) {

        Order order = orderRepository
                .findFirstByOrderIdIdOrderByOrderIdVersionDesc(orderId)
                .orElseThrow(NotExistingOrderException::new);

        order.setStatus(status);

        int version = order.getOrderId().getVersion() + 1;
        order.getOrderId().setVersion(version);

        return orderRepository.save(order);
    }

    public List<Order> getOrders() {
        return orderRepository.findAll();
    }

    public Optional<Order> getCurrentOrderById(long id) {
        return orderRepository.findFirstByOrderIdIdOrderByOrderIdVersionDesc(id);
    }

    private long getCurrentId() {
        final long INITIAL_ID = 1L;

        return orderRepository.findFirstByOrderByOrderIdIdDesc()
                .map(o -> o.getOrderId().getId() + 1)
                .orElse(INITIAL_ID);
    }
}

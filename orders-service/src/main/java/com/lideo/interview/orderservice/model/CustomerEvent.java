package com.lideo.interview.orderservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerEvent implements Serializable {
    private long orderId;
    private CreditStatus creditStatus;
}
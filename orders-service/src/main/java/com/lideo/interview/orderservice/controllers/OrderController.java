package com.lideo.interview.orderservice.controllers;

import com.lideo.interview.orderservice.dto.IncomingOrderDto;
import com.lideo.interview.orderservice.dto.ReturnOrderDto;
import com.lideo.interview.orderservice.entities.Order;
import com.lideo.interview.orderservice.mappers.IncomingOrderMapper;
import com.lideo.interview.orderservice.mappers.ReturnOrderMapper;
import com.lideo.interview.orderservice.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class OrderController {

    private OrderService orderService;

    private ReturnOrderMapper returnOrderMapper;

    private IncomingOrderMapper incomingOrderMapper;

    public OrderController(@Autowired OrderService orderService,
                           @Autowired ReturnOrderMapper returnOrderMapper,
                           @Autowired IncomingOrderMapper incomingOrderMapper) {
        this.orderService = orderService;
        this.returnOrderMapper = returnOrderMapper;
        this.incomingOrderMapper = incomingOrderMapper;
    }

    @GetMapping
    public ResponseEntity<List<ReturnOrderDto>> getOrders() {
        return ResponseEntity.ok(
                orderService
                        .getOrders()
                        .stream()
                        .map(returnOrderMapper::mapToDto)
                        .collect(Collectors.toList())
        );
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<ReturnOrderDto> getOrderById(@PathVariable long orderId) {

        return orderService.getCurrentOrderById(orderId)
                .map(returnOrderMapper::mapToDto)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<ReturnOrderDto> addOrder(@RequestBody @Valid IncomingOrderDto order) {

        Order createdOrder = orderService.placeOrder(incomingOrderMapper.mapToEntity(order));
        ReturnOrderDto returnOrder = returnOrderMapper.mapToDto(createdOrder);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(returnOrder);
    }
}

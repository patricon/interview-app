package com.lideo.interview.orderservice.dto;

import com.lideo.interview.orderservice.model.OrderStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = false)
@Data
public class ReturnOrderDto {
    private long orderId;
    private long customerId;
    private BigDecimal orderAmount;
    private LocalDateTime orderPlacedDate;
    private LocalDateTime lastStatusChangeDate;
    private OrderStatus orderStatus;
}

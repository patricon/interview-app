package com.lideo.interview.orderservice.services;

import com.lideo.interview.orderservice.model.CustomerEvent;
import com.lideo.interview.orderservice.model.OrderStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerCreditEventListener {

    private Logger logger = LoggerFactory.getLogger(CustomerCreditEventListener.class);

    private OrderService orderService;

    public CustomerCreditEventListener(@Autowired OrderService orderService) {
        this.orderService = orderService;
    }

    @RabbitListener(queues = "#{acceptedCreditQueue.name}")
    public void creditAccepted(CustomerEvent event) {

        logger.info("RECEIVED EVENT");
        logger.info(event.toString());

        try {
            orderService.changeStatus(event.getOrderId(), OrderStatus.ACCEPTED);
            logger.info("ORDER ACCEPTED");
        } catch (Exception e) {
            logger.error("Something went wrong during accepting the order", e);
        }
    }

    @RabbitListener(queues = "#{rejectedCreditQueue.name}")
    public void creditRejected(CustomerEvent event) {

        logger.info("RECEIVED EVENT");
        logger.info(event.toString());

        try {
            orderService.changeStatus(event.getOrderId(), OrderStatus.REJECTED);
            logger.info("ORDER REJECTED");
        } catch (Exception e) {
            logger.error("Something went wrong during accepting the order", e);
        }
    }
}

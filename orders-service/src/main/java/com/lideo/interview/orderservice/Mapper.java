package com.lideo.interview.orderservice;

public interface Mapper<S, T> {
    T mapToDto(S entity);

    S mapToEntity(T dto);
}
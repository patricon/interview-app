package com.lideo.interview.orderservice.model;

public enum CreditStatus {
    INSUFFICIENT, CUSTOMER_NOT_EXIST, REDUCED
}

package com.lideo.interview.orderservice.mappers;

import com.lideo.interview.orderservice.Mapper;
import com.lideo.interview.orderservice.dto.IncomingOrderDto;
import com.lideo.interview.orderservice.entities.Order;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IncomingOrderMapper implements Mapper<Order, IncomingOrderDto> {
    private ModelMapper modelMapper;

    public IncomingOrderMapper(@Autowired ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public IncomingOrderDto mapToDto(Order entity) {
        return modelMapper.map(entity, IncomingOrderDto.class);
    }

    @Override
    public Order mapToEntity(IncomingOrderDto dto) {
        return modelMapper.map(dto, Order.class);
    }
}

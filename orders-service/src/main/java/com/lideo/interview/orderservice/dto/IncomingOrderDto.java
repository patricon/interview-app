package com.lideo.interview.orderservice.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = false)
@Data
public class IncomingOrderDto {

    @NotNull(message = "'customerId' shouldn't be null")
    private long customerId;

    @NotNull(message = "'orderAmount' shouldn't be null")
    @Digits(integer = 10, fraction = 2, message = "orderAmount' must be a number max value xxxxxxxxxx.xx")
    private BigDecimal orderAmount;
}

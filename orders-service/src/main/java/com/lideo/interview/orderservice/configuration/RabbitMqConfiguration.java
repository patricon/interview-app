package com.lideo.interview.orderservice.configuration;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfiguration {

    private final String ORDERS_EXCHANGE = "com.lideo.orders.exchange";

    private final String ACCEPTED_CREDIT_QUEUE = "com.lideo.orders.credit.accepted";

    private final String REJECTED_CREDIT_QUEUE = "com.lideo.orders.credit.rejected";

    @Bean
    public ConnectionFactory connectionFactory() {
        return new CachingConnectionFactory("rabit-mq");
    }

    @Bean
    public AmqpAdmin amqpAdmin() {
        return new RabbitAdmin(connectionFactory());
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(connectionFactory());
        template.setMessageConverter(jsonMessageConverter());
        return template;
    }

    @Bean
    public DirectExchange ordersExchange() {
        return new DirectExchange(ORDERS_EXCHANGE);
    }

    @Bean
    public Queue acceptedCreditQueue() {
        return new Queue(ACCEPTED_CREDIT_QUEUE);
    }

    @Bean
    public Queue rejectedCreditQueue() {
        return new Queue(REJECTED_CREDIT_QUEUE);
    }
}

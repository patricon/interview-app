package com.lideo.interview.orderservice.mappers;

import com.lideo.interview.orderservice.Mapper;
import com.lideo.interview.orderservice.dto.ReturnOrderDto;
import com.lideo.interview.orderservice.entities.Order;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReturnOrderMapper implements Mapper<Order, ReturnOrderDto>, InitializingBean {
    private ModelMapper modelMapper;

    public ReturnOrderMapper(@Autowired ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public ReturnOrderDto mapToDto(Order entity) {
        return modelMapper.map(entity, ReturnOrderDto.class);
    }

    @Override
    public Order mapToEntity(ReturnOrderDto dto) {
        return modelMapper.map(dto, Order.class);
    }

    @Override
    public void afterPropertiesSet() {
        PropertyMap<Order, ReturnOrderDto> addressMap = new PropertyMap<>() {
            protected void configure() {
                map().setOrderId(source.getOrderId().getId());
                map().setOrderPlacedDate(source.getPlacedDate());
                map().setLastStatusChangeDate(source.getChangeDate());
            }
        };

        modelMapper.addMappings(addressMap);
    }
}

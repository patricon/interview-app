DROP TABLE IF EXISTS Addresses;
DROP TABLE IF EXISTS Customers;

CREATE TABLE Customers
(
    id               BIGINT         NOT NULL,
    first_name       VARCHAR(100)   NOT NULL,
    last_name        VARCHAR(100)   NOT NULL,
    available_credit NUMERIC(19, 2) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE Addresses
(
    id            BIGINT       NOT NULL,
    customer_id   BIGINT       NOT NULL,
    street        VARCHAR(100) NOT NULL,
    street_number VARCHAR(10)  NOT NULL,
    postal_code   VARCHAR(6)   NOT NULL,
    city          VARCHAR(50)  NOT NULL,
    country       VARCHAR(50)  NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (customer_id) REFERENCES Customers (id)
);

INSERT
INTO Customers
VALUES (1, 'Patryk', 'Pogoda', 500);

INSERT
INTO Customers
VALUES (2, 'Bartek', 'Pogoda', 100);

INSERT
INTO Customers
VALUES (3, 'Sylwia', 'Zawadzka', 400);

INSERT
INTO Addresses
VALUES (1, 1, 'Mickiewicza', '11', '57-300', 'Kłodzko', 'Polska');

INSERT
INTO Addresses
VALUES (2, 1, 'Prusa', '6/64', '50-391', 'Wrocław', 'Polska');

INSERT
INTO Addresses
VALUES (3, 2, 'Wysockiego', '41', '51-691', 'Wrocław', 'Polska');

INSERT
INTO Addresses
VALUES (4, 3, 'Prusa', '6/64', '50-391', 'Wrocław', 'Polska');
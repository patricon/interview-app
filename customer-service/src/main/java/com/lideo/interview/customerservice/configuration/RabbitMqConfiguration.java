package com.lideo.interview.customerservice.configuration;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfiguration {

    private final String CUSTOMERS_EXCHANGE = "com.lideo.customers.exchange";

    private final String PLACED_ORDERS_EVENT_QUEUE = "com.lideo.customer.placed.orders";

    @Bean
    public ConnectionFactory connectionFactory() {
        return new CachingConnectionFactory("rabit-mq");
    }

    @Bean
    public AmqpAdmin amqpAdmin() {
        return new RabbitAdmin(connectionFactory());
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(connectionFactory());
        template.setMessageConverter(jsonMessageConverter());
        return template;
    }

    @Bean
    public Queue placedOrdersQueue() {
        return new Queue(PLACED_ORDERS_EVENT_QUEUE);
    }

    @Bean
    public DirectExchange customersExchange() {
        return new DirectExchange(CUSTOMERS_EXCHANGE);
    }
}

package com.lideo.interview.customerservice.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.ResourceSupport;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@EqualsAndHashCode(callSuper = false)
@Data
public class SimpleAddressDto extends ResourceSupport {

    private long addressId;

    private long customerId;

    @NotEmpty(message = "'street' shouldn't be empty")
    private String street;

    @NotEmpty(message = "'streetNumber' shouldn't be empty")
    private String streetNumber;

    @NotEmpty(message = "'postalCode' shouldn't be empty")
    @Size(min = 6, max = 6, message = "'postalCode' should have 6 characters (like 57-300)")
    @Pattern(regexp = "[0-9]{2}-[0-9]{3}", message = "'postalCode' should be in format like 57-300")
    private String postalCode;

    @NotEmpty(message = "'city' shouldn't be empty")
    private String city;

    @NotEmpty(message = "'country' shouldn't be empty")
    private String country;

}

package com.lideo.interview.customerservice.services;

import com.lideo.interview.customerservice.entities.Address;
import com.lideo.interview.customerservice.repositories.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService {

    private AddressRepository addressRepository;

    public AddressService(@Autowired AddressRepository addressRepository) {

        this.addressRepository = addressRepository;
    }

    public List<Address> getAddressesForCustomer(long customerId) {

        return addressRepository.findByCustomerId(customerId);
    }

    public Address addAddress(long customerId, Address address) {

        address.setCustomerId(customerId);

        return addressRepository.save(address);
    }
}

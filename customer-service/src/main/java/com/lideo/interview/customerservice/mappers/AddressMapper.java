package com.lideo.interview.customerservice.mappers;

import com.lideo.interview.customerservice.dto.SimpleAddressDto;
import com.lideo.interview.customerservice.entities.Address;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressMapper implements Mapper<Address, SimpleAddressDto>, InitializingBean {

    private ModelMapper modelMapper;

    public AddressMapper(@Autowired ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public SimpleAddressDto mapToDto(Address entity) {

        SimpleAddressDto ret = modelMapper.map(entity, SimpleAddressDto.class);

        ret.setAddressId(entity.getId());

        return ret;
    }

    @Override
    public Address mapToEntity(SimpleAddressDto dto) {
        Address ret = modelMapper.map(dto, Address.class);

        ret.setId(dto.getAddressId());

        return ret;
    }

    @Override
    public void afterPropertiesSet() {

        PropertyMap<SimpleAddressDto, Address> addressMap = new PropertyMap<>() {
            protected void configure() {
                map().setId(source.getAddressId());
            }
        };

        modelMapper.addMappings(addressMap);
    }
}

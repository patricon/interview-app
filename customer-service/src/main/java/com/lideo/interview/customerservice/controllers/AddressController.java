package com.lideo.interview.customerservice.controllers;

import com.lideo.interview.customerservice.dto.SimpleAddressDto;
import com.lideo.interview.customerservice.entities.Address;
import com.lideo.interview.customerservice.mappers.AddressMapper;
import com.lideo.interview.customerservice.services.AddressService;
import com.lideo.interview.customerservice.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/{customerId}/addresses")
public class AddressController {

    private CustomerService customerService;

    private AddressService addressService;

    private AddressMapper addressMapper;

    public AddressController(@Autowired CustomerService customerService,
                             @Autowired AddressService addressService,
                             @Autowired AddressMapper addressMapper) {

        this.customerService = customerService;
        this.addressService = addressService;
        this.addressMapper = addressMapper;
    }


    @GetMapping
    public ResponseEntity<List<SimpleAddressDto>> getAddresses(@PathVariable long customerId) {

        if (customerService.notExists(customerId)) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity
                .ok(addressService.getAddressesForCustomer(customerId)
                        .stream()
                        .map(addressMapper::mapToDto)
                        .map(this::addLinksTo)
                        .collect(Collectors.toList())
                );
    }


    @PostMapping
    public ResponseEntity<SimpleAddressDto> addAddress(@PathVariable long customerId,
                                                       @RequestBody @Valid SimpleAddressDto addressDto) {

        if (customerService.notExists(customerId)) {
            return ResponseEntity.notFound().build();
        }

        Address createdAddress = addressService.addAddress(customerId, addressMapper.mapToEntity(addressDto));

        SimpleAddressDto createdAddressDto = addLinksTo(addressMapper.mapToDto(createdAddress));

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(createdAddressDto);
    }


    private SimpleAddressDto addLinksTo(SimpleAddressDto dto) {

        Link customerLink = linkTo(
                methodOn(CustomerController.class)
                        .getCustomer(dto.getCustomerId()))
                .withRel("customer");

        dto.add(customerLink);

        return dto;
    }
}

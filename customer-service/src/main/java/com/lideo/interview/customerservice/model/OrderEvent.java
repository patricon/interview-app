package com.lideo.interview.customerservice.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class OrderEvent implements Serializable {
    private long orderId;
    private long customerId;
    private BigDecimal orderAmount;
    private OrderStatus previousState;
    private OrderStatus newState;
}

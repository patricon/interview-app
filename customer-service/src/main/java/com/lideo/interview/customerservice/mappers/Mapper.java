package com.lideo.interview.customerservice.mappers;

public interface Mapper<S, T> {
    T mapToDto(S entity);

    S mapToEntity(T dto);
}
package com.lideo.interview.customerservice.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;

@Table(name = "Addresses")
@Entity
@Data
@NoArgsConstructor
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NonNull
    private long customerId;

    @NonNull
    private String street;

    @NonNull
    private String streetNumber;

    @NonNull
    private String postalCode;

    @NonNull
    private String city;

    @NonNull
    private String country;

}

package com.lideo.interview.customerservice.controllers;

import com.lideo.interview.customerservice.dto.SimpleCustomerDto;
import com.lideo.interview.customerservice.entities.Customer;
import com.lideo.interview.customerservice.mappers.CustomerMapper;
import com.lideo.interview.customerservice.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class CustomerController {

    private CustomerService customerService;

    private CustomerMapper customerMapper;

    public CustomerController(
            @Autowired CustomerService customerService,
            @Autowired CustomerMapper customerMapper) {

        this.customerService = customerService;
        this.customerMapper = customerMapper;
    }


    @GetMapping
    public ResponseEntity<List<SimpleCustomerDto>> getCustomers() {

        return ResponseEntity
                .ok(customerService.getAllCustomers()
                        .stream()
                        .map(customerMapper::mapToDto)
                        .map(this::addLinksTo)
                        .collect(Collectors.toList())
                );
    }


    @GetMapping("/{customerId}")
    public ResponseEntity<SimpleCustomerDto> getCustomer(@PathVariable long customerId) {

        return customerService.getCustomerById(customerId)
                .map(customerMapper::mapToDto)
                .map(this::addLinksTo)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }


    @GetMapping("/{customerId}/credit")
    public ResponseEntity<BigDecimal> getCustomersCredit(@PathVariable long customerId) {

        return customerService.getCredit(customerId)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }


    @PostMapping
    public ResponseEntity<SimpleCustomerDto> addCustomer(@RequestBody @Valid SimpleCustomerDto customerDto) {

        Customer createdCustomer = customerService.addCustomer(customerMapper.mapToEntity(customerDto));

        SimpleCustomerDto createdCustomerDto = addLinksTo(customerMapper.mapToDto(createdCustomer));

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(createdCustomerDto);
    }


    private SimpleCustomerDto addLinksTo(SimpleCustomerDto dto) {

        Link selfLink = linkTo(CustomerController.class)
                .slash(dto.getCustomerId())
                .withSelfRel();

        Link addressesLink = linkTo(
                methodOn(AddressController.class)
                        .getAddresses(dto.getCustomerId()))
                .withRel("addresses");

        dto.add(selfLink, addressesLink);

        return dto;
    }
}

package com.lideo.interview.customerservice.repositories;

import com.lideo.interview.customerservice.entities.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {
    @NonNull
    List<Customer> findAll();

    @Query("select c.availableCredit from Customer c where c.id = ?1")
    Optional<BigDecimal> getCreditForCustomer(long id);
}

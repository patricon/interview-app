package com.lideo.interview.customerservice.model;

public enum CreditStatus {
    INSUFFICIENT, CUSTOMER_NOT_EXIST, REDUCED
}

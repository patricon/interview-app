package com.lideo.interview.customerservice.model;

public enum OrderStatus {
    NONE, PLACED, ACCEPTED, DENIED
}

package com.lideo.interview.customerservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class CustomerEvent implements Serializable {
    private long orderId;
    private CreditStatus creditStatus;
}
package com.lideo.interview.customerservice.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.ResourceSupport;

import javax.validation.constraints.NotEmpty;

@EqualsAndHashCode(callSuper = false)
@Data
public class SimpleCustomerDto extends ResourceSupport {

    private long customerId;

    @NotEmpty(message = "'firstName' shouldn't be empty")
    private String firstName;

    @NotEmpty(message = "'lastName' shouldn't be empty")
    private String lastName;

}

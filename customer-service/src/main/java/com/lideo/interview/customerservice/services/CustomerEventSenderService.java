package com.lideo.interview.customerservice.services;

import com.lideo.interview.customerservice.model.CreditStatus;
import com.lideo.interview.customerservice.model.CustomerEvent;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerEventSenderService {

    private DirectExchange customersExchange;

    private RabbitTemplate rabbitTemplate;

    public CustomerEventSenderService(@Autowired DirectExchange customersExchange, @Autowired RabbitTemplate rabbitTemplate) {
        this.customersExchange = customersExchange;
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendEvent(long orderId, CreditStatus status) {
        CustomerEvent event = new CustomerEvent(orderId, status);

        rabbitTemplate.convertAndSend(customersExchange.getName(), status.toString(), event);
    }
}

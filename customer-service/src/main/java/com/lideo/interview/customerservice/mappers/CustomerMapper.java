package com.lideo.interview.customerservice.mappers;

import com.lideo.interview.customerservice.dto.SimpleCustomerDto;
import com.lideo.interview.customerservice.entities.Customer;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerMapper implements Mapper<Customer, SimpleCustomerDto> {

    private ModelMapper modelMapper;

    public CustomerMapper(@Autowired ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public SimpleCustomerDto mapToDto(Customer entity) {

        SimpleCustomerDto ret = modelMapper.map(entity, SimpleCustomerDto.class);

        ret.setCustomerId(entity.getId());

        return ret;
    }

    @Override
    public Customer mapToEntity(SimpleCustomerDto dto) {

        Customer ret = modelMapper.map(dto, Customer.class);

        ret.setId(dto.getCustomerId());

        return ret;
    }
}

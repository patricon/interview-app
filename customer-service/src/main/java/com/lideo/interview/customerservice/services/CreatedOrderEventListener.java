package com.lideo.interview.customerservice.services;

import com.lideo.interview.customerservice.exceptions.IncorrectCustomerIdException;
import com.lideo.interview.customerservice.exceptions.InsufficientCreditException;
import com.lideo.interview.customerservice.model.CreditStatus;
import com.lideo.interview.customerservice.model.OrderEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class CreatedOrderEventListener {

    private Logger logger = LoggerFactory.getLogger(CreatedOrderEventListener.class);

    private CustomerService customerService;

    private CustomerEventSenderService customerEventSenderService;

    public CreatedOrderEventListener(@Autowired CustomerService customerService, @Autowired CustomerEventSenderService customerEventSenderService) {
        this.customerService = customerService;
        this.customerEventSenderService = customerEventSenderService;
    }

    @RabbitListener(queues = "#{placedOrdersQueue.name}")
    public void checkCustomerCredit(OrderEvent in) {

        logger.info("RECEIVED ORDER TO CHECK");
        logger.info(in.toString());

        long customerId = in.getCustomerId();
        long orderId = in.getOrderId();
        BigDecimal amount = in.getOrderAmount();

        try {
            customerService.reduceCredit(customerId, amount);
            logger.info("CUSTOMERS CREDIT REDUCED");
            customerEventSenderService.sendEvent(orderId, CreditStatus.REDUCED);
        } catch (InsufficientCreditException e) {
            logger.error("CUSTOMER HAS NOT SUFFICIENT CREDIT");
            customerEventSenderService.sendEvent(orderId, CreditStatus.INSUFFICIENT);

        } catch (IncorrectCustomerIdException e) {
            logger.error("CUSTOMER DOESN'T EXIST");
            customerEventSenderService.sendEvent(orderId, CreditStatus.CUSTOMER_NOT_EXIST);
        }
    }
}

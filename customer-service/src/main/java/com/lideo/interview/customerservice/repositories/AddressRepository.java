package com.lideo.interview.customerservice.repositories;

import com.lideo.interview.customerservice.entities.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends CrudRepository<Address, Long> {

    List<Address> findByCustomerId(long customerId);

}

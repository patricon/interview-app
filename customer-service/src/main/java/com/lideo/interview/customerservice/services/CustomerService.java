package com.lideo.interview.customerservice.services;

import com.lideo.interview.customerservice.entities.Customer;
import com.lideo.interview.customerservice.exceptions.IncorrectCustomerIdException;
import com.lideo.interview.customerservice.exceptions.InsufficientCreditException;
import com.lideo.interview.customerservice.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    private static final BigDecimal INITIAL_CREDIT = BigDecimal.valueOf(500);

    private CustomerRepository customerRepository;

    public CustomerService(@Autowired CustomerRepository customerRepository) {

        this.customerRepository = customerRepository;
    }

    public List<Customer> getAllCustomers() {

        return customerRepository.findAll();
    }

    public Optional<Customer> getCustomerById(long id) {

        return customerRepository.findById(id);
    }

    public Customer addCustomer(Customer customer) {

        customer.setAvailableCredit(INITIAL_CREDIT);

        return customerRepository.save(customer);
    }

    public boolean notExists(long customerId) {

        return !customerRepository.existsById(customerId);
    }

    public void reduceCredit(long customerId, BigDecimal amount) throws InsufficientCreditException, IncorrectCustomerIdException {

        Customer customer = getCustomerById(customerId)
                .orElseThrow(IncorrectCustomerIdException::new);

        if (customer.getAvailableCredit().compareTo(amount) < 0) {
            throw new InsufficientCreditException();
        }

        customer.setAvailableCredit(
                customer.getAvailableCredit().subtract(amount)
        );

        customerRepository.save(customer);
    }

    public Optional<BigDecimal> getCredit(long customerId) {
        return customerRepository.getCreditForCustomer(customerId);
    }
}

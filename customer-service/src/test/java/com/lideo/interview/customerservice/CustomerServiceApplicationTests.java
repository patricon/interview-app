package com.lideo.interview.customerservice;

import com.lideo.interview.customerservice.controllers.AddressController;
import com.lideo.interview.customerservice.controllers.CustomerController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceApplicationTests {

    @Autowired
    AddressController addressController;

    @Autowired
    CustomerController customerController;

    @Test
    public void contextLoads() {
        assertThat(addressController).isNotNull();
        assertThat(customerController).isNotNull();
    }
}

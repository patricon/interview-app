package com.lideo.interview.customerservice.mappers;

import com.lideo.interview.customerservice.dto.SimpleAddressDto;
import com.lideo.interview.customerservice.entities.Address;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AddressMapperTests {

    @Autowired
    AddressMapper addressMapper;

    @Test
    public void contextLoads() {
        assertThat(addressMapper).isNotNull();
    }

    @Test
    public void should_ProperlyConvert_EntityToDto() {

        //arrange
        Address address = new Address();

        address.setId(1);
        address.setCustomerId(1);
        address.setStreet("testStreet");
        address.setStreetNumber("11");
        address.setPostalCode("51-692");
        address.setCity("testCity");
        address.setCountry("Poland");

        //act
        SimpleAddressDto dto = addressMapper.mapToDto(address);

        //assert
        assertThat(dto.getAddressId()).isEqualTo(address.getId());
        assertThat(dto.getCustomerId()).isEqualTo(address.getCustomerId());
        assertThat(dto.getStreet()).isEqualTo(address.getStreet());
        assertThat(dto.getStreetNumber()).isEqualTo(address.getStreetNumber());
        assertThat(dto.getPostalCode()).isEqualTo(address.getPostalCode());
        assertThat(dto.getCity()).isEqualTo(address.getCity());
        assertThat(dto.getCountry()).isEqualTo(address.getCountry());

    }

    @Test
    public void should_ProperlyConvert_DtoToEntity() {

        //arrange
        SimpleAddressDto dto = new SimpleAddressDto();

        dto.setAddressId(1);
        dto.setCustomerId(1);
        dto.setStreet("testStreet");
        dto.setStreetNumber("11");
        dto.setPostalCode("51-692");
        dto.setCity("testCity");
        dto.setCountry("Poland");

        //act
        Address address = addressMapper.mapToEntity(dto);

        //assert
        assertThat(address.getId()).isEqualTo(dto.getAddressId());
        assertThat(address.getCustomerId()).isEqualTo(dto.getCustomerId());
        assertThat(address.getStreet()).isEqualTo(dto.getStreet());
        assertThat(address.getStreetNumber()).isEqualTo(dto.getStreetNumber());
        assertThat(address.getPostalCode()).isEqualTo(dto.getPostalCode());
        assertThat(address.getCity()).isEqualTo(dto.getCity());
        assertThat(address.getCountry()).isEqualTo(dto.getCountry());
    }
}

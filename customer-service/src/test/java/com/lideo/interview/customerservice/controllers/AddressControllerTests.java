package com.lideo.interview.customerservice.controllers;

import com.lideo.interview.customerservice.dto.SimpleAddressDto;
import com.lideo.interview.customerservice.entities.Address;
import com.lideo.interview.customerservice.mappers.AddressMapper;
import com.lideo.interview.customerservice.mappers.CustomerMapper;
import com.lideo.interview.customerservice.services.AddressService;
import com.lideo.interview.customerservice.services.CustomerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AddressController.class)
public class AddressControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CustomerService customerService;

    @MockBean
    private AddressService addressService;

    @MockBean
    private AddressMapper addressMapper;

    @MockBean
    private CustomerMapper customerMapper;

    @Test
    public void should_ReturnHttpNotFound_WhenCustomerWithGivenIdNotExist() throws Exception {
        //arrange
        final long MISSING_ID = 2;

        Mockito.when(customerService.notExists(MISSING_ID)).thenReturn(true);

        //act and assert
        mvc.perform(
                get(MISSING_ID + "/addresses/")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void should_ReturnHttpOkAndListOfAddressesDto_WhenCustomerWithGivenIdExist() throws Exception {
        //arrange
        final int EXISTING_ID = 2;
        final int ADDRESS_ID = 55;

        Mockito.when(customerService.notExists(EXISTING_ID)).thenReturn(false);

        Address address = new Address();
        address.setCustomerId(EXISTING_ID);
        address.setId(ADDRESS_ID);
        address.setStreet("test");

        SimpleAddressDto addressDto = new SimpleAddressDto();
        addressDto.setCustomerId(EXISTING_ID);
        addressDto.setAddressId(ADDRESS_ID);
        addressDto.setStreet("test");

        Mockito.when(addressService.getAddressesForCustomer(EXISTING_ID)).thenReturn(Arrays.asList(address));
        Mockito.when(addressMapper.mapToDto(address)).thenReturn(addressDto);

        //act and assert
        mvc.perform(
                get("/" + EXISTING_ID + "/addresses/")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].customerId", is(EXISTING_ID)))
                .andExpect(jsonPath("$[0].addressId", is(ADDRESS_ID)));
    }

}

package com.lideo.interview.customerservice.repositiories;

import com.lideo.interview.customerservice.entities.Address;
import com.lideo.interview.customerservice.entities.Customer;
import com.lideo.interview.customerservice.repositories.AddressRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AddressRepositoryTests {

    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void should_ProperlyFind_AddressByCustomerId() {
        //arrange
        Customer customer = new Customer();
        customer.setFirstName("name");
        customer.setLastName("name");
        customer = entityManager.persistAndFlush(customer);

        final long CUSTOMER_ID = customer.getId();

        Address address = new Address();
        address.setCustomerId(CUSTOMER_ID);
        entityManager.persistAndFlush(address);

        //act
        List<Address> found = addressRepository.findByCustomerId(CUSTOMER_ID);

        //assert
        assertThat(found).allSatisfy(a ->
                assertThat(a.getCustomerId()).isEqualTo(CUSTOMER_ID));

    }
}

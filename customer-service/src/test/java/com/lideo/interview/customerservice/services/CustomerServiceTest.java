package com.lideo.interview.customerservice.services;

import com.lideo.interview.customerservice.entities.Customer;
import com.lideo.interview.customerservice.repositories.CustomerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceTest {

    @Autowired
    private CustomerService customerService;
    @MockBean
    private CustomerRepository customerRepository;

    @Test
    public void should_returnCustomer_WhenCustomerExists() {
        //arrange
        final long EXISTING_ID = 1;

        Customer c = new Customer();
        c.setId(EXISTING_ID);

        Mockito.when(customerRepository.findById(EXISTING_ID))
                .thenReturn(Optional.of(c));

        //act
        Optional<Customer> opt = customerService.getCustomerById(EXISTING_ID);

        //assert
        assertThat(opt.isPresent()).isTrue();
        assertThat(opt.get().getId()).isEqualTo(EXISTING_ID);
    }

    @Test
    public void should_returnEmptyOptional_WhenCustomerNotExists() {
        //arrange
        final long MISSING_ID = 2;

        Mockito.when(customerRepository.findById(MISSING_ID))
                .thenReturn(Optional.empty());

        //act
        Optional<Customer> opt = customerService.getCustomerById(MISSING_ID);

        //assert
        assertThat(!opt.isPresent()).isTrue();
    }

    @Test
    public void should_returnAddedCustomer_WithNewId() {
        //arrange
        final String ADDED_NAME = "Added";
        final long ADDED_ID = 55;

        Customer c1 = new Customer();
        c1.setId(ADDED_ID);

        Mockito.when(customerRepository.save(ArgumentMatchers.argThat(a -> a.getFirstName().equals(ADDED_NAME))))
                .thenReturn(c1);

        Customer c2 = new Customer();
        c2.setFirstName(ADDED_NAME);
        c2.setLastName("test");

        //act
        Customer ret = customerService.addCustomer(c2);

        //assert
        assertThat(ret.getId()).isEqualTo(ADDED_ID);
    }
}
